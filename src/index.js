import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import * as serviceWorker from './serviceWorker';
import 'bootstrap/dist/css/bootstrap.css';
import { BrowserRouter as Router, Route } from "react-router-dom";
import 'bootswatch/dist/darkly/bootstrap.css';
import CharacterCardsPage from './containers/CharacterCards/CardsPage/CardsPage';
import CharacterCardPage from './containers/CharacterCards/CardPage/CardPage';
import LocationCardsPage from './containers/LocationCards/CardsPage/CardsPage';
import LocationCardPage from './containers/LocationCards/CardPage/CardPage';

ReactDOM.render(
    <Router>
        <App>
            <Route exact path="/" component={CharacterCardsPage} />
            <Route path="/character/:id" component={CharacterCardPage} />
            <Route exact path="/location" component={LocationCardsPage} />
            <Route path="/location/:id" component={LocationCardPage} /> 
        </App> 
    </Router>, 
    document.getElementById('root')
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
