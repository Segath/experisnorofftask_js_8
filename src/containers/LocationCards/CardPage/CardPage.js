import React from "react"
import LocationCard from "../../../components/LocationCard/LocationCard"

class CardPage extends React.Component{
    constructor(props){
        super(props);
        this.state = {
            location : {},
        }
    }

    getLocationData = () => {
        fetch("https://rickandmortyapi.com/api/location/" + this.props.match.params.id)
        .then(response => response.json())
        .then(data => {
            if(data){
                this.setState({location: data})
            }
        })
    }

    componentDidMount() {
        this.getLocationData();
    }

    render() {
        let locationCard = null;
        if (this.state.location.id) {
            locationCard = (
            <LocationCard
                key={this.state.location.id}

                {... this.state.location}
            ></LocationCard>
            )
        } else {
            locationCard = <p>Loading...</p>
        }
        return (
            <>
                <div className="container">
                    <div className="row">
                        {locationCard}
                    </div>
                </div>
            </>
        )
    }
}

export default CardPage;