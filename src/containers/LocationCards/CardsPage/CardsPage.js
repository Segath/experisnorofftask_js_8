import React from "react"
import LocationCard from "../../../components/LocationCard/LocationCard"
import Search from "../../../components/Search/Search";

class CardsPage extends React.Component {
    constructor(props){
        super(props)
        this.state = {
            rickMorty: [],
            locationCard: [],
        }
    }

    getData = (filter) => {
        fetch("https://rickandmortyapi.com/api/location/?name=" + filter)
        .then(response => response.json())
        .then(data => {
            if(data.results){
                this.setState({locationCard: data.results})
            }
        })
    
    }

    componentDidMount() {
        this.getData("");
    }

    render(){
        let locations = null;
        if (this.state.locationCard.length > 0){
            locations = this.state.locationCard.map(location => (
                <LocationCard key={location.id} {... location} showLink={true}></LocationCard>
            ));
        } else {
            locations = <p>Loading...</p>
        }

        return (
            <>
                <div className="container">
                    <Search onClick={(filter) => {this.getData(filter)}}/>
                    <br />
                    
                        <div className="row">
                            {locations}
                        </div>
                </div>
                
            </>
        )
    }
}

export default CardsPage