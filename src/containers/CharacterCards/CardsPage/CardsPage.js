import React from "react"
import CharacterCard from "../../../components/CharacterCard/CharacterCard"
import Search from "../../../components/Search/Search";

class CardsPage extends React.Component {
    constructor(props){
        super(props)
        this.state = {
            rickMorty: [],
            characterCards: [],
        }
    }

    getData = (filter) => {
        fetch("https://rickandmortyapi.com/api/character/?name=" + filter)
        .then(response => response.json())
        .then(data => {
            if(data.results){
                this.setState({characterCards: data.results})
            }
        })
    }

    componentDidMount() {
        this.getData("");
    }

    render(){
        let characters = null;
        if (this.state.characterCards.length > 0){
            characters = this.state.characterCards.map(character => (
                <CharacterCard key={character.id} {... character} showLink={true}></CharacterCard>
            ));
        } else {
            characters = <p>Loading...</p>
        }

        return (
            <>
                <div className="container">
                    <Search onClick={(filter) => {
                        this.getData(filter)}}
                    />
                    <br />
                    
                        <div className="row">
                            {characters}
                        </div>
                </div>
                
            </>
        )
    }
}

export default CardsPage