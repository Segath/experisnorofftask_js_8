import React from "react"
import CharacterCard from "../../../components/CharacterCard/CharacterCard"

class CardPage extends React.Component{
    constructor(props){
        super(props);
        this.state = {
            character : {},
        }
    }

    getCharacterData = () => {
        fetch("https://rickandmortyapi.com/api/character/" + this.props.match.params.id)
        .then(response => response.json())
        .then(data => {
            if(data){
                this.setState({character: data})
            }
        })
    }

    componentDidMount() {
        this.getCharacterData();
    }

    render() {
        let characterCard = null;
        if (this.state.character.id) {
            characterCard = (
            <CharacterCard
                key={this.state.character.id}

                {... this.state.character}
            ></CharacterCard>
            )
        } else {
            characterCard = <p>Loading...</p>
        }
        return (
            <>
                <div className="container">
                    <div className="row">
                        {characterCard}
                    </div>
                </div>
            </>
        )
    }
}

export default CardPage;