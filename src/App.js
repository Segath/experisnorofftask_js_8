import React from 'react';
import './App.css';
import {NavLink} from "react-router-dom"

class App extends React.Component{
  constructor(props){
    super(props)
    this.state = {

    }
  }

  render() {
    return (
      <div className="App">
        <nav className="navbar navbar-expand-lg">
          <div className="navbar-list navbar-collapse">
            <NavLink exact to="/" className="nav-link" activeClassName="chosen">Character</NavLink>
            <NavLink to="/location" className="nav-link" activeClassName="chosen">Location</NavLink>
          </div>
        </nav>
 
        <header>
          <img src="https://i2.wp.com/www.pixelcrumb.com/wp-content/uploads/2016/10/RICK-AND-MORTY-BANNER.jpg?fit=1920%2C720"
          style= {{ "margin": "0", 
                    "display": "block", 
                    "width": "100%"
                  }}
          alt="Rick and Morty Banner"
          />
        </header>
        
        <div>
          {this.props.children}
        </div>
        
      </div>
    );
  }
}

export default App;
