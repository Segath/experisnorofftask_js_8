import React from "react"
import { Link } from "react-router-dom";

var linkButton = null;

const characterCard = (props) => {
    linkButton = 
    <Link 
    to={{ pathname: "/location/" + props.id, }}
    className="btn btn-secondary">
        View
    </Link>

    return (
        <div className="col-xs-12 col-sm-6 col-md-4  col-lg-3">
            <div className="card CharacterCard text-white bg-primary mb-3">

                <div className="card-body">
                    <h5 className="card-title">{props.name}</h5>
                    <b>Type: </b> {props.type} <br />
                    <b>Dimension: </b> {props.dimension} <br />
                    <br />
                    {props.showLink ? linkButton : null}
                </div>
            </div>
        </div>
    )
}

export default characterCard