import React from "react"

class CharacterSearch extends React.Component {
    constructor(props){
        super(props)
        this.state = {
            filter: "",
        }
        this.readFilterText = this.readFilterText.bind(this);
        this.handleClick = this.handleClick.bind(this);
    }

    handleClick (){
        if(this.props.onClick) {
            this.props.onClick(this.state.filter)
        }
    }

    readFilterText(e) {
        this.setState({filter: e.target.value})
    }

    render() {
        return (
            <div className="input-group mb-3 col-12" style={{marginTop: "20px"}}>
                <input type="text"
                onChange={this.readFilterText}
                className="form-control"
                placeholder="Search for a Rick &
                Morty characater" />
                <div className="input-group-append">
                    <button onClick={this.handleClick} className="btn btn-primary">
                        Search!
                    </button>
                </div>
            </div>
        );
    }
}

export default CharacterSearch;