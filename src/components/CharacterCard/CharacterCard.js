import React from "react"
import { Link } from "react-router-dom";

var linkButton = null;

const characterCard = (props) => {
    linkButton = 
    <Link className="text-center"
    to={{ pathname: "/character/" + props.id, }}
    className="btn btn-primary">
        View
    </Link>

    return (
        <div className="col-xs-12 col-sm-6 col-md-4 col-lg-3 py-3">
            <div className="card CharacterCard text-white bg-secondary mb-3 h-100 text-left">
                <img src={props.image} alt={props.name} className="card-img-top" />

                <div className="card-body">
                    <h5 className="card-title">{props.name}</h5>
                    <b>Species: </b> {props.species} <br />
                    <b>Status: </b> {props.status} <br />
                    <b>Gender: </b> {props.gender} <br />
                    <b>Location: </b> {props.location.name} <br />
                    <b>Place of origin</b> {props.origin.name} <br />
                    <br />
                    {props.showLink ? linkButton : null}
                </div>

            </div>
        </div>
    )
}

export default characterCard